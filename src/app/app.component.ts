import { Component } from '@angular/core';
import { MatIconRegistry } from "@angular/material/icon";
import { DomSanitizer } from "@angular/platform-browser";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer){
      this.matIconRegistry.addSvgIcon(
        "thumbs-up-solid",
        this.domSanitizer.bypassSecurityTrustResourceUrl("../assets/icons/thumbs-up-solid.svg")
      );
  }

  title = 'front-tmdb';
}
