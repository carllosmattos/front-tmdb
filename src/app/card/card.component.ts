import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { Movie } from '../interface/movie';
import { MovieService } from '../service/movie.service';
import { MatDialog } from '@angular/material/dialog';
import { MoviedialogComponent } from '../moviedialog/moviedialog.component';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  constructor(
    private movieService: MovieService,
    public dialog: MatDialog,
    public authService: AuthService,
    private router: Router
  ) {}

  type: string = '';
  publicPathImage: string = 'https://image.tmdb.org/t/p/w220_and_h330_face';
  movies: Movie[] = [];
  moviesRt: Movie[] = [];
  rating: number = 0;
  rateMovieId: string = '';
  userIdRateMovie: string = '';
  userId: string = '';

  @Input()
  set typeList(typeList: string) {
    if (typeList === 'getTen') {
      this.type = typeList;
    } else if (typeList === 'getRated') {
      this.type = typeList;
    }
  }

  ngOnInit() {
    if (this.type === 'getTen') {
      this.authService
        .GetByEmail(sessionStorage.getItem('email'))
        .subscribe((res: any) => {
          this.userId = res._id.toString();

          this.movieService.GetRated(this.userId).subscribe((res: any) => {
            this.movies = [];
            this.moviesRt = [];
            this.movieService.GetTenMostPopularInBrazil().subscribe(
              (response: any) => {
                res.forEach((result: any) => {
                  response.forEach((data: Movie) => {
                    this.movies.push({
                      id: data.id,
                      original_title: data.original_title,
                      overview: data.overview,
                      vote_average: data.vote_average,
                      poster_path: data.poster_path,
                      rating:
                        result.id === data.id.toString() ? result.rating : '',
                    });
                  });
                });
              },
              (error) => {
                console.log(
                  '🚀 ~ file: card.component.ts:56 ~ CardComponent ~ ngOnInit ~ error:',
                  error
                );
                if (error.status === 401) {
                  this.router.navigate(['login']);
                }
              }
            );
          });
        });
    } else if (this.type === 'getRated') {
      this.authService
        .GetByEmail(sessionStorage.getItem('email'))
        .subscribe((res: any) => {
          this.userId = res._id.toString();

          this.movies = [];
          this.movieService.GetRated(this.userId).subscribe(
            (response: any) => {
              console.log('🚀 ~ GETRATED:', response);
              response.forEach((data: Movie) => {
                this.movies.push({
                  id: data.id,
                  original_title: data.original_title,
                  overview: data.overview,
                  vote_average: data.vote_average,
                  poster_path: data.poster_path,
                  rating: data.rating,
                });
              });
            },
            (error) => {
              console.log(
                '🚀 ~ file: card.component.ts:79 ~ CardComponent ~ ngOnInit ~ error:',
                error
              );
              if (error.status === 401) {
                this.router.navigate(['login']);
              }
            }
          );
        });
    }
  }

  formatLabel(value: number): string {
    this.rating = value / 10;
    return `${value / 10}`;
  }

  rate(rateMovieId: string, inputRate: string) {
    this.rating = Number(inputRate) / 10;
    this.rateMovieId = rateMovieId;
    this.authService
      .GetByEmail(sessionStorage.getItem('email'))
      .subscribe((response: any) => {
        this.userIdRateMovie = response._id;
        this.movieService
          .RateMovie(this.rateMovieId, this.rating, this.userIdRateMovie)
          .subscribe();
      });
  }

  openDialog(id: string) {
    const dialogRef = this.dialog.open(MoviedialogComponent, {
      data: {
        id,
      },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
