import { Component } from '@angular/core';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent {
  nome = '';
  constructor(private authService: AuthService, private router: Router) {}

  email = sessionStorage.getItem('email');

  ngOnInit() {
    this.authService.GetByEmail(this.email).subscribe((res: any) => {
      console.log("🚀 ~ file: home.component.ts:18 ~ HomeComponent ~ this.authService.GetByEmail ~ res:", res)
      this.nome = res.name
    },
    (err) => {
      if(err.status === 401 && sessionStorage.getItem('access_token') === null) {
        this.router.navigate(['login'])
      }
    })
  };
}
