export interface Movie {
  id: string;
  original_title: string;
  overview: string;
  vote_average: number;
  poster_path: string;
  rating: number;
}

export interface Details {
  id: string
  genres: string
  original_title: string
  overview: string
  release_date: string
  runtime: number
  popularity: number
  vote_average: number
  poster_path: string
  rating?: number;
}
