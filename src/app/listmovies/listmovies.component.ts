import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-listmovies',
  templateUrl: './listmovies.component.html',
  styleUrls: ['./listmovies.component.css']
})
export class ListmoviesComponent {

  constructor() { }

  @Input() type: string = '';
}
