import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../service/auth.service';
import { FormBuilder, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(
    private builder: FormBuilder,
    private service: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  loginForm = this.builder.group({
    email: this.builder.control(
      '',
      Validators.compose([Validators.required, Validators.email])
    ),
    password: this.builder.control(
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern(
          '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
        ),
      ])
    ),
  });

  proceedLogin() {
    if (this.loginForm.valid) {
      this.service.Login(this.loginForm.value).subscribe((res: any )=> {
        if (res.access_token) {
          sessionStorage.setItem('email', this.loginForm.value.email || '')
          sessionStorage.setItem('access_token', res.access_token)
          this.router.navigate([''])
        } else {
          this._snackBar.open('Invalid password', 'close', {
            duration: 3000,
          });
        }
      })
    } else {
      this._snackBar.open('Invalid credentials', 'close', {
        duration: 3000,
      });
    }
  }
}
