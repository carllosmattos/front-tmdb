import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { Details, Movie } from '../interface/movie';
import { MovieService } from '../service/movie.service';

@Component({
  selector: 'app-moviedialog',
  templateUrl: './moviedialog.component.html',
  styleUrls: ['./moviedialog.component.css']
})
export class MoviedialogComponent {
  constructor(
    private movieService: MovieService,
    public dialogRef: MatDialogRef<MoviedialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Movie,
  ) {}

  publicPathImage: string = 'https://image.tmdb.org/t/p/w600_and_h900_bestv2';
  movie: Details = {
    id: '',
    genres: '',
    original_title: '',
    overview: '',
    release_date: '',
    runtime: 0,
    popularity: 0,
    vote_average: 0,
    poster_path: '',
    rating: 0,
  };

  ngOnInit() {
    this.movieService.GetMovieDetailsById(this.data.id).subscribe(
      (response: any) => {
        this.movie = response;
      },
      (error) => {
        console.log(
          '🚀 ~ file: card.component.ts:33 ~ CardComponent ~ ngOnInit ~ error:',
          error
        );
      }
    );
  }

}
