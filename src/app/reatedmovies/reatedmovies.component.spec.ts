import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReatedmoviesComponent } from './reatedmovies.component';

describe('ReatedmoviesComponent', () => {
  let component: ReatedmoviesComponent;
  let fixture: ComponentFixture<ReatedmoviesComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ReatedmoviesComponent]
    });
    fixture = TestBed.createComponent(ReatedmoviesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
