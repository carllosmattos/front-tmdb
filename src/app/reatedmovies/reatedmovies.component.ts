import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-reatedmovies',
  templateUrl: './reatedmovies.component.html',
  styleUrls: ['./reatedmovies.component.css']
})
export class ReatedmoviesComponent {

  constructor() { }

  @Input() type: string = '';
}
