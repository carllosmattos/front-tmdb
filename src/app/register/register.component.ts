import { Component, inject } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../service/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent {
  constructor(
    private builder: FormBuilder,
    private service: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  registerForm = this.builder.group({
    name: this.builder.control('', Validators.required),
    email: this.builder.control(
      '',
      Validators.compose([Validators.required, Validators.email])
    ),
    password: this.builder.control(
      '',
      Validators.compose([
        Validators.required,
        Validators.pattern(
          '(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-zd$@$!%*?&].{8,}'
        ),
      ])
    ),
  });

  proceedRegistration() {
    if (this.registerForm.valid) {
      this.service.ProceedRegister(this.registerForm.value).subscribe(res => {
        this._snackBar.open('Registered Successfully', 'close', {
          duration: 3000
        });
        this.router.navigate(['login']);
      });
    } else {
      this._snackBar.open('Please enter valid data', 'close', {
        duration: 3000,
      });
    }
  }
}
