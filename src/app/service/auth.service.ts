import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const options: { headers: HttpHeaders, params?: { region: string } } = {
  headers: new HttpHeaders().set(
    'Authorization',
    `Bearer ${sessionStorage.getItem('access_token')}`
  )
};

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  apiUrl = 'http://localhost:3000';

  GetAll() {
    return this.http.get(`${this.apiUrl}/users`, options)
  }

  GetByEmail(email: any) {
    return this.http.get(`${this.apiUrl}/users/${email}`, options)
  }

  ProceedRegister(inputData: any) {
    return this.http.post(`${this.apiUrl}/users`, inputData)
  }

  Login(loginData: any) {
    return this.http.post(`${this.apiUrl}/auth/login`, loginData)
  }
}
