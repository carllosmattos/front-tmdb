import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

const headers = new HttpHeaders().set(
  'Authorization',
  `Bearer ${sessionStorage.getItem('access_token')}`
);

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  constructor(private http: HttpClient) {}

  apiUrl = 'http://localhost:3000';

  GetTenMostPopularInBrazil() {
    const options = {
      headers,
      params: {
        region: 'BR',
      },
    };
    return this.http.get(`${this.apiUrl}/movies/10-popular-in-brasil`, options);
  }

  GetMovieDetailsById(id: string) {
    const options = { headers };
    return this.http.get(`${this.apiUrl}/movies/movie-details/${id}`, options);
  }

  GetRated(userId: string) {
    const options = { headers, params: { userId } };
    return this.http.get(`${this.apiUrl}/movies/top-rated/`, options);
  }

  RateMovie(movieId: string, rating: number, userIdRateMovie: string) {
    const options = {
      headers,
    };
    const body = {
      rating,
    };
    return this.http.post(
      `${this.apiUrl}/movies/${movieId}?userId=${userIdRateMovie}`,
      body,
      options
    );
  }
}
